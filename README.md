## Sposób uruchomienia aplikacji:

1) Zbudować aplikacje przy pomocy Maven w głównym folderze projektu (podczas budowy aplikacji wykonywane są oprócz normalnych testów również live testy)

    mvn clean install
    
2) Przejść do folderu w którym znajduje się zbudowana aplikacja

    cd pharmaceutical-wholesaler/target
    
3) Uruchomić aplikacje java (Spring Boot)

    java -jar pharmaceutical-wholesaler-0.0.1-SNAPSHOT.jar
    

## Dostęp do aplikacji z przeglądarki internetowej przy pomocy Swagger:

    GUI do API jest dostępne pod adresem:
    localhost:8080/pharmaceuticalwholesaler/swagger-ui/index.html
    

## Opis działania API:

Za pomocą api możemy dodawać i listować produkty, które mają określoną cenę.
Następnie owymi produktami możemy zasilać/redukować stan magazynu, którym są przechowywane.
Po utworzeniu klientów w aplikacji mogą oni zakupywać dotępne w magazynie produkty.
Po udanym zakupie stan magazynu ulega odpowiedniemu pomniejszeniu.
W przypadku próby zakupu produktu, ktorego nie ma na stanie bądź nie jest obslugiwany przez magazyn zostaniemy
poinformowani przy pomocy odpowieniego komunikatu wraz z statusem odpowiedzi HTTP 404 Not Found.