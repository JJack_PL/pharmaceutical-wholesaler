package com.javateam.pharmaceuticalwholesaler.web;

import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import com.javateam.pharmaceuticalwholesaler.model.Address;
import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.Sale;
import com.javateam.pharmaceuticalwholesaler.model.SaleItem;
import com.javateam.pharmaceuticalwholesaler.model.StockState;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class SaleLiveTest {

    private final static String URI = "http://localhost:8080/pharmaceuticalwholesaler/sales";

    private final static Random RANDOM_INT_GENERATOR = new Random();

    @Test
    public void whenCreatingSale_then201Created() {
        // precondition stock state with enough quantity
        Product persistedProductA = createPersitedProduct("prod A", "description A", BigDecimal.valueOf(10));
        @SuppressWarnings("unused")
        StockState persistedStockStateA = createPersitedStockState(persistedProductA, 10);
        Customer persistedCustomer = createPersitedCustomer("firstName", "lastName");

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(new SaleItem(persistedProductA, 3, BigDecimal.valueOf(20)));

        Sale sale = new Sale(LocalDateTime.now(), persistedCustomer, saleItems, BigDecimal.valueOf(100));

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(sale).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(201));
    }

    @Test
    public void whenCreatingSale_thenSaleHasId() {
        // precondition stock state with enough quantity
        Product persistedProductA = createPersitedProduct("prod A", "description A", BigDecimal.valueOf(10));
        @SuppressWarnings("unused")
        StockState persistedStockStateA = createPersitedStockState(persistedProductA, 10);
        Customer persistedCustomer = createPersitedCustomer("firstName", "lastName");

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(new SaleItem(persistedProductA, 3, BigDecimal.valueOf(20)));

        Sale sale = new Sale(LocalDateTime.now(), persistedCustomer, saleItems, BigDecimal.valueOf(100));

        Sale persistedSale = RestAssured.given().contentType(ContentType.JSON).body(sale).post(URI).as(Sale.class);

        assertThat(persistedSale.getId(), Matchers.notNullValue());
    }

    @Test
    public void whenCreatingSale_then400BadRequest_whenNotValidData() {
        // precondition stock state with enough quantity
        Product persistedProductA = createPersitedProduct("prod A", "description A", BigDecimal.valueOf(10));
        @SuppressWarnings("unused")
        StockState persistedStockStateA = createPersitedStockState(persistedProductA, 10);
        Customer persistedCustomer = createPersitedCustomer("firstName", "lastName");

        // empty collection is not allowed
        Set<SaleItem> saleItems = new LinkedHashSet<>();
        Sale sale = new Sale(LocalDateTime.now(), persistedCustomer, saleItems, BigDecimal.valueOf(100));

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(sale).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(400));
    }

    @Test
    public void whenCreatingSale_then404NotFound_whenNotEnoughOnStockState() {
        // precondition stock state with enough quantity
        Product persistedProductA = createPersitedProduct("prod A", "description A", BigDecimal.valueOf(10));
        Customer persistedCustomer = createPersitedCustomer("firstName", "lastName");

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(new SaleItem(persistedProductA, 3, BigDecimal.valueOf(20)));

        Sale sale = new Sale(LocalDateTime.now(), persistedCustomer, saleItems, BigDecimal.valueOf(100));

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(sale).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(404));
    }

    @Test
    public void whenFindByCustomer_then200Ok() {
        // precondition stock state with enough quantity
        Product persistedProductA = createPersitedProduct("prod A", "description A", BigDecimal.valueOf(10));
        @SuppressWarnings("unused")
        StockState persistedStockStateA = createPersitedStockState(persistedProductA, 10);
        Customer persistedCustomerX = createPersitedCustomer("firstName", "lastName");
        Customer persistedCustomerY = createPersitedCustomer("firstName2", "lastName2");

        Set<SaleItem> saleItems1 = new LinkedHashSet<>();
        saleItems1.add(new SaleItem(persistedProductA, 3, BigDecimal.valueOf(20)));
        Set<SaleItem> saleItems2 = new LinkedHashSet<>();
        saleItems2.add(new SaleItem(persistedProductA, 5, BigDecimal.valueOf(20)));

        Sale sale1 = new Sale(LocalDateTime.now(), persistedCustomerX, saleItems1, BigDecimal.valueOf(100));
        Sale persistedSale1 = RestAssured.given().contentType(ContentType.JSON).body(sale1).post(URI).as(Sale.class);

        Sale sale2 = new Sale(LocalDateTime.now(), persistedCustomerY, saleItems2, BigDecimal.valueOf(100));
        @SuppressWarnings("unused")
        Sale persistedSale2 = RestAssured.given().contentType(ContentType.JSON).body(sale2).post(URI).as(Sale.class);

        Response findByCustomerResponse = RestAssured.given().contentType(ContentType.JSON)
            .get(URI + "/history/customer/" + persistedCustomerX.getId());
        Sale[] salesByCustomer = findByCustomerResponse.as(Sale[].class);

        assertThat(findByCustomerResponse.getStatusCode(), Matchers.equalTo(200));
        assertThat(salesByCustomer.length, Matchers.equalTo(1));
        assertThat(salesByCustomer[0], Matchers.equalTo(persistedSale1));
    }

    @Test
    public void whenFindByProduct_then200Ok() {
        // precondition stock state with enough quantity
        Product persistedProductA = createPersitedProduct("prod A", "description A", BigDecimal.valueOf(10));
        Product persistedProductB = createPersitedProduct("prod B", "description B", BigDecimal.valueOf(10));
        @SuppressWarnings("unused")
        StockState persistedStockStateA = createPersitedStockState(persistedProductA, 10);
        @SuppressWarnings("unused")
        StockState persistedStockStateB = createPersitedStockState(persistedProductB, 10);
        Customer persistedCustomer = createPersitedCustomer("firstName", "lastName");

        Set<SaleItem> saleItems1 = new LinkedHashSet<>();
        saleItems1.add(new SaleItem(persistedProductA, 3, BigDecimal.valueOf(20)));
        Set<SaleItem> saleItems2 = new LinkedHashSet<>();
        saleItems2.add(new SaleItem(persistedProductB, 5, BigDecimal.valueOf(20)));

        Sale sale1A = new Sale(LocalDateTime.now(), persistedCustomer, saleItems1, BigDecimal.valueOf(100));
        @SuppressWarnings("unused")
        Sale persistedSale1A = RestAssured.given().contentType(ContentType.JSON).body(sale1A).post(URI).as(Sale.class);

        Sale sale2B = new Sale(LocalDateTime.now(), persistedCustomer, saleItems2, BigDecimal.valueOf(100));
        Sale persistedSale2B = RestAssured.given().contentType(ContentType.JSON).body(sale2B).post(URI).as(Sale.class);

        Response findByProductResponse = RestAssured.given().contentType(ContentType.JSON)
            .get(URI + "/history/product/" + persistedProductB.getId());
        Sale[] salesByProduct = findByProductResponse.as(Sale[].class);

        assertThat(findByProductResponse.getStatusCode(), Matchers.equalTo(200));
        assertThat(salesByProduct.length, Matchers.equalTo(1));
        assertThat(salesByProduct[0], Matchers.equalTo(persistedSale2B));
    }

    private Customer createPersitedCustomer(final String firstName, final String lastName) {
        Address address = new Address("street", "postcode", "city", "country");
        Customer customer = new Customer(firstName, lastName, randomMail(), address);
        return RestAssured.given().contentType(ContentType.JSON).body(customer)
            .post(CustomerLiveTest.URI).as(Customer.class);
    }

    private Product createPersitedProduct(final String name, final String description, final BigDecimal price) {
        Product product = new Product(name, description, price);
        return RestAssured.given().contentType(ContentType.JSON).body(product).post(ProductLiveTest.URI)
            .as(Product.class);
    }

    private StockState createPersitedStockState(final Product persistedProduct, final int quantity) {
        StockState stockState = new StockState(persistedProduct, quantity);
        return RestAssured.given().contentType(ContentType.JSON).body(stockState).post(StockStateLiveTest.URI)
            .as(StockState.class);
    }

    private String randomMail() {
        return "my" + RANDOM_INT_GENERATOR.nextInt() + "@gmail.com";
    }

}
