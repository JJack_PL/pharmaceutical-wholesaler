package com.javateam.pharmaceuticalwholesaler.web;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;
import java.util.Random;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import com.javateam.pharmaceuticalwholesaler.model.Address;
import com.javateam.pharmaceuticalwholesaler.model.Customer;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class CustomerLiveTest {

    public final static String URI = "http://localhost:8080/pharmaceuticalwholesaler/customers";

    private final static Random RANDOM_INT_GENERATOR = new Random();

    @Test
    public void whenCreatingCustomer_then201Created() {
        Address address = new Address("street", "postcode", "city", "country");
        Customer customer = new Customer("firstName", "lastName", randomMail(), address);

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(customer).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(201));
    }

    @Test
    public void whenCreatingCustomer_thenProductHasId() {
        Address address = new Address("street", "postcode", "city", "country");
        Customer customer = new Customer("firstName", "lastName", randomMail(), address);

        Customer persistedCustomer = RestAssured.given().contentType(ContentType.JSON).body(customer)
            .post(URI).as(Customer.class);
        assertThat(persistedCustomer.getId(), Matchers.notNullValue());
    }

    @Test
    public void whenAllCustomersAreRetrieved_then200OK() {
        Response response = RestAssured.given().accept(ContentType.JSON).get(URI);

        assertThat(response.getStatusCode(), Matchers.equalTo(200));
    }

    @Test
    public void whenAllCustomersAreRetrieved_thenAtLeastOneProductExists() {
        Address address = new Address("street", "postcode", "city", "country");
        Customer customer = new Customer("firstName", "lastName", randomMail(), address);

        @SuppressWarnings("unused")
        Customer persistedCustomer = RestAssured.given().contentType(ContentType.JSON).body(customer)
            .post(URI).as(Customer.class);

        Response response = RestAssured.given().accept(ContentType.JSON).get(URI);
        List<Customer> customers = response.as(List.class);

        assertThat(customers, not(Matchers.<Customer>empty()));
    }

    private String randomMail() {
        return "my" + RANDOM_INT_GENERATOR.nextInt() + "@gmail.com";
    }

}
