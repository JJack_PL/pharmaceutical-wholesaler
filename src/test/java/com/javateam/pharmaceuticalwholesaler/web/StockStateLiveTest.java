package com.javateam.pharmaceuticalwholesaler.web;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.StockState;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class StockStateLiveTest {

    public final static String URI = "http://localhost:8080/pharmaceuticalwholesaler/stockStates";

    @Test
    public void whenGetExistingStockStateByProduct_then200OK() {
        Product product = new Product("prod A", "description A", BigDecimal.valueOf(10));
        Product persistedProduct =
            RestAssured.given().contentType(ContentType.JSON).body(product).post(ProductLiveTest.URI).as(Product.class);

        StockState stockState = new StockState(persistedProduct, 10);
        @SuppressWarnings("unused")
        StockState createdStockState =
            RestAssured.given().contentType(ContentType.JSON).body(stockState).post(URI).as(StockState.class);

        Response responseGetByProduct =
            RestAssured.given().accept(ContentType.JSON).get(URI + "/product/" + persistedProduct.getId());
        assertThat(responseGetByProduct.getStatusCode(), Matchers.equalTo(200));
    }

    @Test
    public void whenGetStockStateByNotExistingProduct_then404NotFound() {
        long nonExistingProductId = 123456789L;

        Response responseGetByProduct =
            RestAssured.given().accept(ContentType.JSON).get(URI + "/product/" + nonExistingProductId);
        assertThat(responseGetByProduct.getStatusCode(), Matchers.equalTo(404));
    }

    @Test
    public void whenGetNotExistingStockStateByExistingProduct_then404NotFound() {
        Product productA = new Product("prod A", "description A", BigDecimal.valueOf(10));
        Product persistedProductA =
            RestAssured.given().contentType(ContentType.JSON).body(productA).post(ProductLiveTest.URI)
                .as(Product.class);
        Product productB = new Product("prod B", "description B", BigDecimal.valueOf(10));
        Product persistedProductB =
            RestAssured.given().contentType(ContentType.JSON).body(productB).post(ProductLiveTest.URI)
                .as(Product.class);

        StockState stockStateA = new StockState(persistedProductA, 10);
        @SuppressWarnings("unused")
        StockState createdStockStateA =
            RestAssured.given().contentType(ContentType.JSON).body(stockStateA).post(URI).as(StockState.class);

        Response responseGetByProduct =
            RestAssured.given().accept(ContentType.JSON).get(URI + "/product/" + persistedProductB.getId());
        assertThat(responseGetByProduct.getStatusCode(), Matchers.equalTo(404));
    }

    @Test
    public void whenCreatingStockState_then201Created() {
        Product product = new Product("prod", "description", BigDecimal.valueOf(5));
        Product persistedProduct =
            RestAssured.given().contentType(ContentType.JSON).body(product).post(ProductLiveTest.URI)
                .as(Product.class);
        StockState stockState = new StockState(persistedProduct, 10);

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(stockState).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(201));
    }

    @Test
    public void whenCreatingStockState_then400BadRequest_nullValue() {
        Response createResponse = RestAssured.given().contentType(ContentType.JSON).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(400));
    }

    @Test
    public void whenCreatingStockState_then400BadRequest_idIsDefined() {
        Product product = new Product("prod", "description", BigDecimal.valueOf(5));
        Product persistedProduct =
            RestAssured.given().contentType(ContentType.JSON).body(product).post(ProductLiveTest.URI)
                .as(Product.class);
        StockState stockState = new StockState(persistedProduct, 10);
        stockState.setId(new Random().nextLong());

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(stockState).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(400));
    }

    @Test
    public void whenCreatingStockState_thenStockStateHasId() {
        Product product = new Product("prod", "description", BigDecimal.valueOf(5));
        Product persistedProduct =
            RestAssured.given().contentType(ContentType.JSON).body(product).post(ProductLiveTest.URI)
                .as(Product.class);
        StockState stockState = new StockState(persistedProduct, 10);

        StockState persistedStockState =
            RestAssured.given().contentType(ContentType.JSON).body(stockState).post(URI).as(StockState.class);
        assertThat(persistedStockState.getId(), Matchers.notNullValue());
    }

    @Test
    public void whenAllStockStatesAreRetrieved_then200OK() {
        Response response = RestAssured.given().accept(ContentType.JSON).get(URI);

        assertThat(response.getStatusCode(), Matchers.equalTo(200));
    }

    @Test
    public void whenAllStockStatesAreRetrieved_thenAtLeastOneStockStateExists() {
        Product product = new Product("prod A", "description A", BigDecimal.valueOf(10));
        RestAssured.given().contentType(ContentType.JSON).body(product).post(URI);

        Response response = RestAssured.given().accept(ContentType.JSON).get(URI);
        List<StockState> stockStates = response.as(List.class);

        assertThat(stockStates, not(Matchers.<StockState>empty()));
    }

    @Test
    public void whenUpdatingStockState_then200Ok() {
        Product productA = new Product("prod A", "description A", BigDecimal.valueOf(5));
        Product persistedProductA =
            RestAssured.given().contentType(ContentType.JSON).body(productA).post(ProductLiveTest.URI)
                .as(Product.class);
        StockState stockState = new StockState(persistedProductA, 10);

        StockState persistedStockState =
            RestAssured.given().contentType(ContentType.JSON).body(stockState).post(URI).as(StockState.class);

        Product productB = new Product("prod B", "description B", BigDecimal.valueOf(5));
        Product persistedProductB =
            RestAssured.given().contentType(ContentType.JSON).body(productB).post(ProductLiveTest.URI)
                .as(Product.class);

        persistedStockState.setProduct(persistedProductB);
        persistedStockState.setQuantity(15);

        Response updateResponse = RestAssured.given().contentType(ContentType.JSON)
            .body(persistedStockState)
            .put(URI + "/" + persistedStockState.getId());
        assertThat(updateResponse.getStatusCode(), Matchers.equalTo(200));
        assertThat(persistedStockState, Matchers.equalTo(updateResponse.as(StockState.class)));
    }

}
