package com.javateam.pharmaceuticalwholesaler.web;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import com.javateam.pharmaceuticalwholesaler.model.Product;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class ProductLiveTest {

    public final static String URI = "http://localhost:8080/pharmaceuticalwholesaler/products";

    @Test
    public void whenCreatingProduct_then201Created() {
        Product product = new Product("prod A", "description A", BigDecimal.valueOf(10));

        Response createResponse = RestAssured.given().contentType(ContentType.JSON).body(product).post(URI);
        assertThat(createResponse.getStatusCode(), Matchers.equalTo(201));
    }

    @Test
    public void whenCreatingProduct_thenProductHasId() {
        Product product = new Product("prod A", "description A", BigDecimal.valueOf(10));

        Product createdProduct =
            RestAssured.given().contentType(ContentType.JSON).body(product).post(URI).as(Product.class);

        assertThat(createdProduct.getId(), Matchers.notNullValue());
    }

    @Test
    public void whenAllProductsAreRetrieved_then200Ok() {
        Response response = RestAssured.given().accept(ContentType.JSON).get(URI);

        assertThat(response.getStatusCode(), Matchers.equalTo(200));
    }

    @Test
    public void whenAllProductsAreRetrieved_thenAtLeastOneProductExists() {
        Product product = new Product("prod A", "description A", BigDecimal.valueOf(10));
        RestAssured.given().contentType(ContentType.JSON).body(product).post(URI);

        Response response = RestAssured.given().accept(ContentType.JSON).get(URI);
        List<Product> products = response.as(List.class);

        assertThat(products, not(Matchers.<Product>empty()));
    }

}
