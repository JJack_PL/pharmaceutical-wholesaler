package com.javateam.pharmaceuticalwholesaler.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.CustomerDao;
import com.javateam.pharmaceuticalwholesaler.domainutil.DomainCreator;
import com.javateam.pharmaceuticalwholesaler.model.Address;
import com.javateam.pharmaceuticalwholesaler.model.Customer;

@SpringBootTest
@Transactional
public class CustomerServiceTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    DomainCreator domainCreator;

    @Autowired
    CustomerService customerService;

    @Autowired
    CustomerDao customerDao;

    @Test
    public void testCreateCustomer() {
        String firstName = "Jacek";
        String lastName = "Kazmierczak";
        String email = "testmail@gmail.com";
        Address address = new Address("street", "postcode", "city", "country");
        Customer customer = new Customer(firstName, lastName, email, address);

        Customer persistedCustomer = this.customerService.createCustomer(customer);

        this.entityManager.flush();
        this.entityManager.clear();

        Optional<Customer> result = this.customerDao.findById(persistedCustomer.getId());
        assertTrue(result.isPresent());
        assertEquals(persistedCustomer, result.get());
    }

    @Test
    public void testFindAll() {
        String firstNameA = "Jacek";
        String lastNameA = "Kazmierczak";
        String emailA = "testmail@gmail.com";
        Address addressA = new Address("street", "postcode", "city", "country");

        String firstNameB = "Jacek2";
        String lastNameB = "Kazmierczak2";
        String emailB = "testmail2@gmail.com";
        Address addressB = new Address("street2", "postcode2", "city2", "country2");

        Customer customerA = this.domainCreator.createCustomer(firstNameA, lastNameA, emailA, addressA);
        Customer customerB = this.domainCreator.createCustomer(firstNameB, lastNameB, emailB, addressB);

        this.entityManager.flush();
        this.entityManager.clear();

        List<Customer> result = this.customerService.findAll();
        assertTrue(result.contains(customerA));
        assertTrue(result.contains(customerB));
    }

}
