package com.javateam.pharmaceuticalwholesaler.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.StockStateDao;
import com.javateam.pharmaceuticalwholesaler.dao.exception.MyEntityNotFoundException;
import com.javateam.pharmaceuticalwholesaler.domainutil.DomainCreator;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.StockState;
import com.javateam.pharmaceuticalwholesaler.service.exception.NotEnoughProductOnStockException;

@SpringBootTest
@Transactional
public class StockStateServiceTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    DomainCreator domainCreator;

    @Autowired
    StockStateService stockStateService;

    @Autowired
    StockStateDao stockStateDao;

    @Test
    public void testFindAll() {
        Product productA = this.domainCreator.createProduct("product A", BigDecimal.valueOf(10.5));
        Product productB = this.domainCreator.createProduct("product B", BigDecimal.valueOf(20.5));
        Product productC = this.domainCreator.createProduct("product C", BigDecimal.valueOf(30.5));
        StockState stockStateA = this.domainCreator.createStockState(productA, 10);
        StockState stockStateB = this.domainCreator.createStockState(productB, 20);
        StockState stockStateC = this.domainCreator.createStockState(productC, 30);

        this.entityManager.flush();
        this.entityManager.clear();

        List<StockState> result = this.stockStateService.findAll();
        assertTrue(result.contains(stockStateA));
        assertTrue(result.contains(stockStateB));
        assertTrue(result.contains(stockStateC));
    }

    @Test
    public void testRemoveProductFromStockState_valid() throws NotEnoughProductOnStockException {
        int startQuantity = 10;
        int quantityToRemove = 4;
        Product product = this.domainCreator.createProduct("product A", BigDecimal.valueOf(3));
        StockState stockState = this.domainCreator.createStockState(product, startQuantity);

        int remainingQuantity = this.stockStateService.removeProductFromStockState(product, quantityToRemove);

        this.entityManager.flush();
        this.entityManager.clear();

        StockState reloaded = this.stockStateDao.findById(stockState.getId()).get();

        assertEquals(startQuantity - quantityToRemove, remainingQuantity);
        assertEquals(startQuantity - quantityToRemove, reloaded.getQuantity());
    }

    @Test
    public void testRemoveProductFromStockState_notRegisteredOnStock() throws NotEnoughProductOnStockException {
        Product product = this.domainCreator.createProduct("product A", BigDecimal.valueOf(3));
        int quantityToRemove = 4;

        NotEnoughProductOnStockException thrown = assertThrows(NotEnoughProductOnStockException.class, () -> {
            this.stockStateService.removeProductFromStockState(product, quantityToRemove);
        }, "NotEnoughProductOnStockException was expected");

        assertEquals("There is no stock state for the product: " + product, thrown.getMessage());
    }

    @Test
    public void testRemoveProductFromStockState_notEnoughProductsOnStock() throws NotEnoughProductOnStockException {
        int startQuantity = 10;
        int quantityToRemove = 12;
        Product product = this.domainCreator.createProduct("product A", BigDecimal.valueOf(3));
        @SuppressWarnings("unused")
        StockState stockState = this.domainCreator.createStockState(product, startQuantity);

        NotEnoughProductOnStockException thrown = assertThrows(NotEnoughProductOnStockException.class, () -> {
            this.stockStateService.removeProductFromStockState(product, quantityToRemove);
        }, "NotEnoughProductOnStockException was expected");

        String expectedMessage = "There is only " + startQuantity + " products on stock so there is no way to remove "
            + quantityToRemove + " from it. "
            + product;
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    public void testGetByProduct_found() {
        Product product = this.domainCreator.createProduct("product A", BigDecimal.valueOf(3));
        StockState stockState = this.domainCreator.createStockState(product, 10);

        this.entityManager.flush();
        this.entityManager.clear();

        StockState reloaded = this.stockStateService.getByProduct(product);
        assertEquals(stockState, reloaded);
    }

    @Test
    public void testGetByProduct_notFound() {
        Product productA = this.domainCreator.createProduct("product A", BigDecimal.valueOf(3));
        Product productB = this.domainCreator.createProduct("product B", BigDecimal.valueOf(3));
        @SuppressWarnings("unused")
        StockState stockStateA = this.domainCreator.createStockState(productA, 10);

        this.entityManager.flush();
        this.entityManager.clear();

        assertThrows(MyEntityNotFoundException.class, () -> {
            this.stockStateService.getByProduct(productB);
        }, "MyEntityNotFoundException was expected");
    }

    @Test
    public void testCreateStockState() {
        int quantity = 10;
        Product product = this.domainCreator.createProduct("product", BigDecimal.valueOf(3));
        StockState stockState = new StockState(product, quantity);

        StockState persistedStockState = this.stockStateService.createStockState(stockState);

        this.entityManager.flush();
        this.entityManager.clear();

        StockState reloaded = this.stockStateDao.findById(persistedStockState.getId()).get();
        assertEquals(persistedStockState, reloaded);
    }

    @Test
    public void testUpdateStockState() {
        Product oldProduct = this.domainCreator.createProduct("product A", BigDecimal.valueOf(3));
        Product newProduct = this.domainCreator.createProduct("product B", BigDecimal.valueOf(3));
        int oldQuantity = 5;
        int newQuantity = 6;

        StockState stockState = this.domainCreator.createStockState(oldProduct, oldQuantity);

        this.entityManager.flush();
        this.entityManager.clear();

        stockState.setProduct(newProduct);
        stockState.setQuantity(newQuantity);
        this.stockStateService.updateStockState(stockState);

        this.entityManager.flush();
        this.entityManager.clear();

        StockState reloaded = this.stockStateDao.findById(stockState.getId()).get();
        assertEquals(stockState, reloaded);
    }

}
