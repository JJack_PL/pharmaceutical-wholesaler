package com.javateam.pharmaceuticalwholesaler.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.SaleDao;
import com.javateam.pharmaceuticalwholesaler.dao.SaleItemDao;
import com.javateam.pharmaceuticalwholesaler.dao.StockStateDao;
import com.javateam.pharmaceuticalwholesaler.domainutil.DomainCreator;
import com.javateam.pharmaceuticalwholesaler.model.Address;
import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.Sale;
import com.javateam.pharmaceuticalwholesaler.model.SaleItem;
import com.javateam.pharmaceuticalwholesaler.model.StockState;
import com.javateam.pharmaceuticalwholesaler.service.exception.StockStateException;

@SpringBootTest
@Transactional
public class SaleServiceTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    DomainCreator domainCreator;

    @Autowired
    SaleService saleService;

    @Autowired
    SaleDao saleDao;

    @Autowired
    SaleItemDao saleItemDao;

    @Autowired
    StockStateDao stockStateDao;

    @Test
    public void testCreateSale_valid() {
        // precondition: prepare stock states
        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10));
        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(20));

        StockState stockStateA = this.domainCreator.createStockState(productA, 10);
        StockState stockStateB = this.domainCreator.createStockState(productB, 10);

        Customer customer = this.domainCreator.createCustomer("firstName", "lastName", "email@o2.pl");

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(new SaleItem(productA, 5, BigDecimal.valueOf(50)));
        saleItems.add(new SaleItem(productB, 6, BigDecimal.valueOf(120)));

        Sale sale = new Sale(LocalDateTime.now(), customer, saleItems, BigDecimal.valueOf(100.5));
        Sale persistedSale = this.saleService.createSale(sale);

        this.entityManager.flush();
        this.entityManager.clear();

        Optional<Sale> result = this.saleDao.findById(persistedSale.getId());
        assertTrue(result.isPresent());
        assertEquals(persistedSale, result.get());

        List<SaleItem> persistedSaleItems = this.saleItemDao.findAll();
        assertEquals(2, persistedSaleItems.size()); // 53
        assertTrue(result.get().getSaleItems().containsAll(persistedSaleItems));

        assertEquals(5, this.stockStateDao.findById(stockStateA.getId()).get().getQuantity());
        assertEquals(4, this.stockStateDao.findById(stockStateB.getId()).get().getQuantity());
    }

    @Test
    public void testCreateSale_notEnoughProductsInStock() {
        // precondition: prepare stock states
        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10));
        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(20));

        @SuppressWarnings("unused")
        StockState stockStateA = this.domainCreator.createStockState(productA, 10);
        @SuppressWarnings("unused")
        StockState stockStateB = this.domainCreator.createStockState(productB, 10);

        Customer customer = this.domainCreator.createCustomer("firstName", "lastName", "email@o2.pl");

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(new SaleItem(productA, 12, BigDecimal.valueOf(50)));
        saleItems.add(new SaleItem(productB, 13, BigDecimal.valueOf(120)));

        Sale sale = new Sale(LocalDateTime.now(), customer, saleItems, BigDecimal.valueOf(100.5));

        StockStateException thrown = assertThrows(StockStateException.class, () -> {
            this.saleService.createSale(sale);
        }, "StockStateException was expected");

        assertEquals("There is only 10 products on stock so there is no way to remove 12 from it. " + productA,
            thrown.getNotEnoughProductOnStockExceptions().get(0).getMessage());
        assertEquals("There is only 10 products on stock so there is no way to remove 13 from it. " + productB,
            thrown.getNotEnoughProductOnStockExceptions().get(1).getMessage());
    }

    @Test
    public void testCreateSale_notRegisteredProductInStock() {
        // precondition: prepare stock states
        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10));
        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(20));

        @SuppressWarnings("unused")
        StockState stockStateA = this.domainCreator.createStockState(productA, 10);

        Customer customer = this.domainCreator.createCustomer("firstName", "lastName", "email@o2.pl");

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(new SaleItem(productA, 3, BigDecimal.valueOf(50)));
        saleItems.add(new SaleItem(productB, 6, BigDecimal.valueOf(120)));

        Sale sale = new Sale(LocalDateTime.now(), customer, saleItems, BigDecimal.valueOf(100.5));

        StockStateException thrown = assertThrows(StockStateException.class, () -> {
            this.saleService.createSale(sale);
        }, "StockStateException was expected");

        assertEquals("There is no stock state for the product: " + productB,
            thrown.getNotEnoughProductOnStockExceptions().get(0).getMessage());
        assertEquals(0, this.saleItemDao.count());
    }

    @Test
    public void testFindAll() {
        LocalDateTime dateTimeA = LocalDateTime.now();
        LocalDateTime dateTimeB = dateTimeA.plusMinutes(10);
        Customer customer = this.domainCreator.createCustomer("firstName", "lastName", "email@o2.pl");

        // precondition: prepare stock states
        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10));
        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(30));
        @SuppressWarnings("unused")
        StockState stockStateA = this.domainCreator.createStockState(productA, 10);
        @SuppressWarnings("unused")
        StockState stockStateB = this.domainCreator.createStockState(productB, 10);

        Set<SaleItem> saleItems1 = new LinkedHashSet<>();
        saleItems1.add(new SaleItem(productA, 2, BigDecimal.valueOf(20)));
        saleItems1.add(new SaleItem(productB, 3, BigDecimal.valueOf(90)));
        Set<SaleItem> saleItems2 = new LinkedHashSet<>();
        saleItems2.add(new SaleItem(productA, 2, BigDecimal.valueOf(20)));
        saleItems2.add(new SaleItem(productB, 3, BigDecimal.valueOf(90)));

        Sale sale1 = new Sale(dateTimeA, customer, saleItems1, BigDecimal.valueOf(100.5));
        Sale persistedSale1 = this.saleService.createSale(sale1);
        Sale sale2 = new Sale(dateTimeB, customer, saleItems2, BigDecimal.valueOf(200.5));
        Sale persistedSale2 = this.saleService.createSale(sale2);

        this.entityManager.flush();
        this.entityManager.clear();

        List<Sale> result = this.saleDao.findAll();
        assertTrue(result.contains(persistedSale1));
        assertTrue(result.contains(persistedSale2));
    }

    @Test
    public void testFindByCustomer() {
        Address address = new Address("street", "postcode", "city", "country");
        Customer customerA = this.domainCreator.createCustomer("firstName", "lastName", "email@o2.pl", address);
        Customer customerB = this.domainCreator.createCustomer("firstName2", "lastName2", "email2@o2.pl", address);

        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10));
        int quantityA = 2;
        BigDecimal totalPriceA = BigDecimal.valueOf(20);
        SaleItem saleItemA = new SaleItem(productA, quantityA, totalPriceA);

        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(30));
        int quantityB = 3;
        BigDecimal totalPriceB = BigDecimal.valueOf(90);
        SaleItem saleItemB = new SaleItem(productB, quantityB, totalPriceB);

        Set<SaleItem> saleItems = new LinkedHashSet<>();
        saleItems.add(saleItemA);
        saleItems.add(saleItemB);

        Sale saleA1 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(1), customerA, saleItems,
            BigDecimal.valueOf(10));
        Sale saleA2 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(2), customerA, saleItems,
            BigDecimal.valueOf(15));
        Sale saleB3 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(3), customerB, saleItems,
            BigDecimal.valueOf(20));
        Sale saleA4 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(4), customerA, saleItems,
            BigDecimal.valueOf(25));

        List<Sale> result = this.saleService.findByCustomer(customerA, Sort.by(Direction.DESC, "dateTime"));
        assertEquals(3, result.size());
        assertEquals(saleA4, result.get(0));
        assertEquals(saleA2, result.get(1));
        assertEquals(saleA1, result.get(2));
    }

    @Test
    public void testFindByProductDescDateTime() {
        Address address = new Address("street", "postcode", "city", "country");
        Customer customer = this.domainCreator.createCustomer("firstName", "lastName", "email@o2.pl", address);

        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10));
        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(30));
        Product productC = this.domainCreator.createProduct("prod C", BigDecimal.valueOf(40));

        Set<SaleItem> saleItemsAB1 = new LinkedHashSet<>();
        saleItemsAB1.add(new SaleItem(productA, 2, BigDecimal.valueOf(20)));
        saleItemsAB1.add(new SaleItem(productB, 3, BigDecimal.valueOf(90)));

        Set<SaleItem> saleItemsA2 = new LinkedHashSet<>();
        saleItemsA2.add(new SaleItem(productA, 2, BigDecimal.valueOf(20)));

        Set<SaleItem> saleItemsB3 = new LinkedHashSet<>();
        saleItemsB3.add(new SaleItem(productB, 3, BigDecimal.valueOf(90)));

        Set<SaleItem> saleItemsCA4 = new LinkedHashSet<>();
        saleItemsCA4.add(new SaleItem(productC, 2, BigDecimal.valueOf(80)));
        saleItemsCA4.add(new SaleItem(productA, 2, BigDecimal.valueOf(20)));

        Sale saleAB1 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(1), customer, saleItemsAB1,
            BigDecimal.valueOf(10));
        Sale saleA2 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(2), customer, saleItemsA2,
            BigDecimal.valueOf(15));
        Sale saleB3 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(3), customer, saleItemsB3,
            BigDecimal.valueOf(20));
        Sale saleCA4 = this.domainCreator.createSale(LocalDateTime.now().plusMinutes(4), customer, saleItemsCA4,
            BigDecimal.valueOf(25));

        this.entityManager.flush();
        this.entityManager.clear();

        assertEquals(6, this.saleItemDao.findAll().size());

        List<Sale> result = this.saleService.findByProductDescDateTime(productA);
        assertEquals(3, result.size());
        assertEquals(saleCA4, result.get(0));
        assertEquals(saleA2, result.get(1));
        assertEquals(saleAB1, result.get(2));

    }

}
