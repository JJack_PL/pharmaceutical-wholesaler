package com.javateam.pharmaceuticalwholesaler.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.ProductDao;
import com.javateam.pharmaceuticalwholesaler.domainutil.DomainCreator;
import com.javateam.pharmaceuticalwholesaler.model.Product;

@SpringBootTest
@Transactional
public class ProductServiceTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    DomainCreator domainCreator;

    @Autowired
    ProductService productService;

    @Autowired
    ProductDao productDao;

    @Test
    public void testCreateProduct() {
        BigDecimal price = BigDecimal.valueOf(10.5);
        Product product = new Product("product 1", "product 1 description", price);
        Product persistedProduct = this.productService.createProduct(product);

        this.entityManager.flush();
        this.entityManager.clear();

        Optional<Product> result = this.productDao.findById(persistedProduct.getId());
        assertTrue(result.isPresent());
        assertEquals(persistedProduct, result.get());
    }

    @Test
    public void testFindAll() {
        Product productA = this.domainCreator.createProduct("product 1", BigDecimal.valueOf(10.5));
        Product productB = this.domainCreator.createProduct("product 2", BigDecimal.valueOf(20.5));
        Product productC = this.domainCreator.createProduct("product 3", BigDecimal.valueOf(30.5));

        this.entityManager.flush();
        this.entityManager.clear();

        List<Product> result = this.productService.findAll();
        assertTrue(result.contains(productA));
        assertTrue(result.contains(productB));
        assertTrue(result.contains(productC));
    }

}
