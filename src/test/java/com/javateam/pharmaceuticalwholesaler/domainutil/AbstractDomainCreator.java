package com.javateam.pharmaceuticalwholesaler.domainutil;

import java.util.Random;

import com.javateam.pharmaceuticalwholesaler.model.BaseEntity;

/**
 * Base class for each package grouped domains creator.
 */
public abstract class AbstractDomainCreator {

    private final Random idGenerator = new Random();

    protected abstract <T extends BaseEntity> T createEntity(T entity);

    protected Long newId() {
        return this.idGenerator.nextLong();
    }

}
