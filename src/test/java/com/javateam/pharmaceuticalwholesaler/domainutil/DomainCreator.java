package com.javateam.pharmaceuticalwholesaler.domainutil;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import com.javateam.pharmaceuticalwholesaler.model.Address;
import com.javateam.pharmaceuticalwholesaler.model.BaseEntity;
import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.Sale;
import com.javateam.pharmaceuticalwholesaler.model.SaleItem;
import com.javateam.pharmaceuticalwholesaler.model.StockState;

public abstract class DomainCreator extends AbstractDomainCreator {

    // ----- Product ----- //

    public Product createProduct(final String name, final BigDecimal price) {
        return createProduct(name, name, price);
    }

    public Product createProduct(final String name, final String description, final BigDecimal price) {
        Product entity = new Product(name, description, price);
        return createEntity(entity);
    }

    // ----- Customer ----- //

    public Customer createCustomer(final String firstName, final String lastName, final String email,
        final Address address) {
        Customer entity = new Customer(firstName, lastName, email, address);
        return createEntity(entity);
    }

    public Customer createCustomer(final String firstName, final String lastName, final String email) {
        Address address = new Address("Sosnowa 10/2", "47-100", "Strzelce Opolskie", "Polska");
        Customer entity = new Customer(firstName, lastName, email, address);
        return createEntity(entity);
    }

    // ----- StockState ----- //

    public StockState createStockState(final Product product, final int quantity) {
        StockState entity = new StockState(product, quantity);
        return createEntity(entity);
    }

    // ----- Sale ----- //

    public Sale createSale(final LocalDateTime dateTime, final Customer customer, final Set<SaleItem> saleItems,
        final BigDecimal totalPrice) {
        Sale entity = new Sale(dateTime, customer, saleItems, totalPrice);
        return createEntity(entity);
    }

    // --------------------------- TRANSIENT ---------------------------- //
    public static final DomainCreator.Transient TRANSIENT = new Transient();

    public static class Transient extends DomainCreator {

        @Override
        protected <T extends BaseEntity> T createEntity(final T entity) {
            return entity;
        }
    }

    // --------------------------- PERSISTENT --------------------------- //
    @Service
    public static class Persistent extends DomainCreator {

        @PersistenceContext
        private EntityManager entityManager;

        @Override
        protected <T extends BaseEntity> T createEntity(final T entity) {
            this.entityManager.persist(entity);
            return entity;
        }
    }

}
