package com.javateam.pharmaceuticalwholesaler.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.domainutil.DomainCreator;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.StockState;

@SpringBootTest
@Transactional
public class StockStateDaoTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    DomainCreator domainCreator;

    @Autowired
    StockStateDao stockStateDao;

    @Test
    public void testFindByProduct() {
        Product productA = this.domainCreator.createProduct("prod A", BigDecimal.valueOf(10.5));
        Product productB = this.domainCreator.createProduct("prod B", BigDecimal.valueOf(20.5));

        StockState stockStateA = this.domainCreator.createStockState(productA, 10);

        this.entityManager.flush();
        this.entityManager.clear();

        assertEquals(stockStateA, this.stockStateDao.findByProduct(productA).get());
        assertFalse(this.stockStateDao.findByProduct(productB).isPresent());
    }

}
