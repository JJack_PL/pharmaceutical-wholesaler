package com.javateam.pharmaceuticalwholesaler.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Entity
public class Customer extends BaseEntity {

    /** The serialVersionUID. */
    private static final long serialVersionUID = 5838068205091498633L;

    /** Default sorting used by spring data. */
    public static final Sort DEFAULT_SORT = Sort.by(Direction.ASC, "lastName", "firstName");

    /** The first name. */
    @Size(max = 20)
    @Column(nullable = false)
    private String firstName;

    /** The last name. */
    @NotEmpty
    @Size(max = 40)
    @Column(nullable = false)
    private String lastName;

    /** The email address. */
    @NotEmpty
    @Email
    @Size(max = 64)
    @Column(nullable = false, unique = true)
    private String email;

    /** The address. */
    @NotNull
    @Valid
    @Embedded
    private Address address;

    /** Exist only for Hibernate to create entity by reflection. Don't use it! */
    protected Customer() {
    }

    /**
     * Instantiates a new customer.
     *
     * @param firstName the first name
     * @param lastName the last name
     * @param email the email
     * @param address the address
     */
    public Customer(final String firstName, final String lastName, final String email, final Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(final Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Customer [" + super.toString() + ", firstName=" + this.firstName + ", lastName=" + this.lastName
            + ", email=" + this.email
            + ", address="
            + this.address + "]";
    }

}
