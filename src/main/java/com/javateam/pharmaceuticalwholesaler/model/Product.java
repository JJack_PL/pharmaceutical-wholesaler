package com.javateam.pharmaceuticalwholesaler.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Entity
public class Product extends BaseEntity {

    /** The serialVersionUID. */
    private static final long serialVersionUID = 4611316937406798033L;

    /** Default sorting used by spring data. */
    public static final Sort DEFAULT_SORT = Sort.by(Direction.ASC, "name");

    @NotEmpty
    @Size(max = 128)
    private String name;

    @NotEmpty
    @Size(max = 255)
    private String description;

    @Digits(fraction = 2, integer = 10)
    @DecimalMin(value = "0", inclusive = false)
    private BigDecimal price;

    /** Exist only for Hibernate to create entity by reflection. Don't use it! */
    protected Product() {
    }

    public Product(final String name, final String description, final BigDecimal price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(final BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product [" + super.toString() + ", name=" + this.name + ", description=" + this.description + ", price="
            + this.price + "]";
    }

}
