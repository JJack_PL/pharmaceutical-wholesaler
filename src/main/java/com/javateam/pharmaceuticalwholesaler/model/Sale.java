package com.javateam.pharmaceuticalwholesaler.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Entity
public class Sale extends BaseEntity {

    /** The serialVersionUID. */
    private static final long serialVersionUID = -5892743327099328698L;

    /** Default sorting used by spring data. */
    public static final Sort DEFAULT_SORT = Sort.by(Direction.DESC, "dateTime");

    /** The date time. */
    private LocalDateTime dateTime;

    /** The customer. */
    @NotNull
    @ManyToOne
    private Customer customer;

    /** The sale items as ordered set. */
    @NotEmpty
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "sale_id")
    private final Set<SaleItem> saleItems = new LinkedHashSet<>();

    /** The total price. */
    @Digits(fraction = 2, integer = 10)
    @DecimalMin(value = "0", inclusive = false)
    private BigDecimal totalPrice;

    /** Exist only for Hibernate to create entity by reflection. Don't use it! */
    protected Sale() {
    }

    /**
     * Instantiates a new sale.
     *
     * @param dateTime the date time
     * @param customer the customer
     * @param saleItems the sale items
     * @param totalPrice the total price
     */
    public Sale(final LocalDateTime dateTime, final Customer customer, final Set<SaleItem> saleItems,
        final BigDecimal totalPrice) {
        this.dateTime = dateTime;
        this.customer = customer;
        this.saleItems.addAll(saleItems);
        this.totalPrice = totalPrice;
    }

    public Set<SaleItem> getSaleItems() {
        return this.saleItems;
    }

    public void setSaleItems(final Set<SaleItem> saleItems) {
        this.saleItems.clear();
        this.saleItems.addAll(saleItems);
    }

    public LocalDateTime getDateTime() {
        return this.dateTime;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    @Override
    public String toString() {
        return "Sale [" + super.toString() + ", dateTime=" + this.dateTime + ", customer=" + this.customer
            + ", saleItems=" + this.saleItems
            + ", totalPrice="
            + this.totalPrice + "]";
    }

}
