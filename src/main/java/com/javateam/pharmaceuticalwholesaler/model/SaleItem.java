package com.javateam.pharmaceuticalwholesaler.model;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Sale items are persisted at the same time when the {@link Sale} is persisted.
 */
@Entity
public class SaleItem extends BaseEntity {

    /** The serialVersionUID. */
    private static final long serialVersionUID = -6630218967892995724L;

    /** The product. */
    @NotNull
    @ManyToOne
    private Product product;

    /** The quantity. */
    @Min(1)
    private int quantity;

    /** The total price. */
    @Digits(fraction = 2, integer = 10)
    @DecimalMin(value = "0", inclusive = false)
    private BigDecimal totalPrice;

    /** Exist only for Hibernate to create entity by reflection. Don't use it! */
    protected SaleItem() {
    }

    /**
     * Instantiates a new sale item.
     *
     * @param product the product
     * @param quantity the quantity
     * @param totalPrice the total price
     */
    public SaleItem(final Product product, final int quantity, final BigDecimal totalPrice) {
        this.product = product;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(final BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * We need to override equals and hashCode methods in order to save a set of {@link SaleItem sale items} without
     * knowing the value of the id at this time.
     *
     * @param obj the obj
     * @return true, if equals
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(this.product, this.quantity, this.totalPrice);
        return result;
    }

    /**
     * We need to override equals and hashCode methods in order to save a set of {@link SaleItem sale items} without
     * knowing the value of the id at this time.
     *
     * @param obj the obj
     * @return true, if equals
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || (getClass() != obj.getClass())) {
            return false;
        }
        SaleItem other = (SaleItem) obj;
        return Objects.equals(this.product, other.product) && this.quantity == other.quantity
            && Objects.equals(this.totalPrice, other.totalPrice);
    }

    @Override
    public String toString() {
        return "SaleItem [" + super.toString() + ", product=" + this.product + ", quantity=" + this.quantity
            + ", totalPrice=" + this.totalPrice
            + "]";
    }

}
