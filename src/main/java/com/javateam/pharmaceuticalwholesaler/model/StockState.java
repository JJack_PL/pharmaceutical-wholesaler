package com.javateam.pharmaceuticalwholesaler.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Entity
public class StockState extends BaseEntity {

    /** The serialVersionUID. */
    private static final long serialVersionUID = 4896067951561802196L;

    /** Default sorting used by spring data. */
    public static final Sort DEFAULT_SORT = Sort.by(Direction.ASC, "product.name");

    /** The product. */
    @NotNull
    @OneToOne(optional = false)
    private Product product;

    /** The quantity. */
    @Min(1)
    private int quantity;

    /** Exist only for Hibernate to create entity by reflection. Don't use it! */
    protected StockState() {
    }

    /**
     * Instantiates a new stock state.
     *
     * @param product the product
     * @param quantity the quantity
     */
    public StockState(final Product product, final int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    /**
     * Adds the quantity to the existing one.
     *
     * @param quantity the quantity
     */
    public void addQuantity(final int quantity) {
        this.quantity += quantity;
    }
    /**
     * Removes the quantity from the existing one.
     *
     * @param quantity the quantity
     */
    public void removeQuantity(final int quantity) {
        this.quantity -= quantity;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "StockState [" + super.toString() + ", product=" + this.product + ", quantity=" + this.quantity + "]";
    }

}
