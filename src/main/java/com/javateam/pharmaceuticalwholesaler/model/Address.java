package com.javateam.pharmaceuticalwholesaler.model;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;

@Embeddable
public class Address {

    /** The street. */
    @NotEmpty
    private String street;

    /** The postcode. */
    @NotEmpty
    private String postcode;

    /** The city. */
    @NotEmpty
    private String city;

    /** The country. */
    @NotEmpty
    private String country;

    /** Exist only for Hibernate to create entity by reflection. Don't use it! */
    protected Address() {
    }

    /**
     * Instantiates a new address.
     *
     * @param street the street
     * @param postcode the postcode
     * @param city the city
     * @param country the country
     */
    public Address(final String street, final String postcode, final String city, final String country) {
        super();
        this.street = street;
        this.postcode = postcode;
        this.city = city;
        this.country = country;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Address [street=" + this.street + ", postcode=" + this.postcode + ", city=" + this.city + ", country="
            + this.country + "]";
    }

}
