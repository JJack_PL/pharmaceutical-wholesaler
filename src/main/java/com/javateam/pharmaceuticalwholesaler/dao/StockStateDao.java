package com.javateam.pharmaceuticalwholesaler.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.StockState;

public interface StockStateDao extends JpaRepository<StockState, Long>, JpaSpecificationExecutor<StockState> {

    Optional<StockState> findByProduct(Product product);
    
}
