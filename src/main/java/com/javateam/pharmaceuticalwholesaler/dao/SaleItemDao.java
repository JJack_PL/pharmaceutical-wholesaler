package com.javateam.pharmaceuticalwholesaler.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.javateam.pharmaceuticalwholesaler.model.SaleItem;

public interface SaleItemDao extends JpaRepository<SaleItem, Long>, JpaSpecificationExecutor<SaleItem> {

}
