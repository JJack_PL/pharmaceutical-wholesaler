package com.javateam.pharmaceuticalwholesaler.dao.exception;

/**
 * The exception when can not find an entity.
 */
public class MyEntityNotFoundException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4948359263186585861L;

    /**
     * Constructs a new <code>EntityNotFoundException</code> exception with the
     * specified detail message.
     *
     * @param message the detail message.
     */
    public MyEntityNotFoundException(final String message) {
        super(message);
    }

}
