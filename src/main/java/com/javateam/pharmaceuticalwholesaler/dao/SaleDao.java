package com.javateam.pharmaceuticalwholesaler.dao;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.Sale;

public interface SaleDao extends JpaRepository<Sale, Long>, JpaSpecificationExecutor<Sale> {

    /**
     * Find by customer with the given sorting.
     *
     * @param sort the sort
     * @return the list of sorted sales
     */
    List<Sale> findByCustomer(Customer customer, Sort sort);

    /**
     * Find by product descending by date time.
     *
     * @param product the product
     * @return the list of sales
     */
    @Query("SELECT s FROM Sale s INNER JOIN s.saleItems si WHERE si.product = :product ORDER BY s.dateTime DESC")
    List<Sale> findByProductDescDateTime(@Param("product") Product product);

}
