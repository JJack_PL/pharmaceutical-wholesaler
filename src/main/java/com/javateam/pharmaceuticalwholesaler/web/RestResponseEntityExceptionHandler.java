package com.javateam.pharmaceuticalwholesaler.web;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.javateam.pharmaceuticalwholesaler.dao.exception.MyEntityNotFoundException;
import com.javateam.pharmaceuticalwholesaler.service.exception.NotEnoughProductOnStockException;
import com.javateam.pharmaceuticalwholesaler.service.exception.StockStateException;
import com.javateam.pharmaceuticalwholesaler.web.exception.MyBadRequestException;

/**
 * Exception handler with different http status responses.
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    // 400

    @Override
    protected final ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex,
        final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        return handleExceptionInternal(ex, message(HttpStatus.BAD_REQUEST, ex), headers, HttpStatus.BAD_REQUEST,
            request);
    }

    @Override
    protected final ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
        final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        ValidationErrorDto validationErrorDto = processFieldErrors(fieldErrors);

        return handleExceptionInternal(ex, validationErrorDto, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Handle 400 bad request.
     *
     * @param ex the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler(value = {MyBadRequestException.class, DataIntegrityViolationException.class,
        ConstraintViolationException.class})
    protected final ResponseEntity<Object> handleBadRequest(final RuntimeException ex, final WebRequest request) {
        return handleExceptionInternal(ex, message(HttpStatus.BAD_REQUEST, ex), new HttpHeaders(),
            HttpStatus.BAD_REQUEST, request);
    }

    // 404

    /**
     * Handle product not found in stock.
     *
     * @param stockStateException the stock state exception
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({StockStateException.class})
    protected ResponseEntity<Object> handleProductNotFoundInStock(final StockStateException stockStateException,
        final WebRequest request) {

        List<ApiError> apiErrors = new ArrayList<>();
        for (NotEnoughProductOnStockException ex : stockStateException.getNotEnoughProductOnStockExceptions()) {
            apiErrors.add(message(HttpStatus.NOT_FOUND, ex));
        }
        return handleExceptionInternal(stockStateException, apiErrors, new HttpHeaders(), HttpStatus.NOT_FOUND,
            request);
    }

    /**
     * Handle not found.
     *
     * @param ex the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({MyEntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {

        ApiError apiError = message(HttpStatus.NOT_FOUND, ex);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    // 500

    /**
     * Handle 500 s.
     *
     * @param ex the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handle500s(final RuntimeException ex, final WebRequest request) {
        final ApiError apiError = message(HttpStatus.INTERNAL_SERVER_ERROR, ex);

        return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    /**
     * Message object.
     *
     * @param httpStatus the http status
     * @param ex the ex
     * @return the api error
     */
    private final ApiError message(final HttpStatus httpStatus, final Exception ex) {
        String message = ex.getMessage() == null ? ex.getClass().getSimpleName() : ex.getMessage();
        String developerMessage = ExceptionUtils.getRootCauseMessage(ex);

        return new ApiError(httpStatus.value(), message, developerMessage);
    }

    /**
     * Process field errors.
     *
     * @param fieldErrors the field errors
     * @return the validation error dto
     */
    private ValidationErrorDto processFieldErrors(final List<FieldError> fieldErrors) {
        ValidationErrorDto validationErrorDto = new ValidationErrorDto();

        for (final FieldError fieldError : fieldErrors) {
            String localizedErrorMessage = fieldError.getDefaultMessage();
            validationErrorDto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }

        return validationErrorDto;
    }

}
