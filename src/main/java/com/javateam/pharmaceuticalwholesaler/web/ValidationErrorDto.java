package com.javateam.pharmaceuticalwholesaler.web;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents validation errors.
 */
public class ValidationErrorDto {

    /** The field errors. */
    private final List<FieldErrorDto> fieldErrors = new ArrayList<>();

    /**
     * Adds the field error.
     *
     * @param path the path
     * @param message the message
     */
    public void addFieldError(final String path, final String message) {
        FieldErrorDto error = new FieldErrorDto(path, message);
        this.fieldErrors.add(error);
    }

    public List<FieldErrorDto> getFieldErrors() {
        return this.fieldErrors;
    }

    @Override
    public String toString() {
        return "ValidationErrorDto [fieldErrors=" + this.fieldErrors + "]";
    }

}
