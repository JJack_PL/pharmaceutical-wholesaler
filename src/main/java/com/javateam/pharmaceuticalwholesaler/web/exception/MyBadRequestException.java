package com.javateam.pharmaceuticalwholesaler.web.exception;

/**
 * Bad request exception
 */
public final class MyBadRequestException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1531234047660264797L;

    /**
     * Instantiates a new my bad request exception.
     */
    public MyBadRequestException() {
        super();
    }

    /**
     * Instantiates a new my bad request exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public MyBadRequestException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new my bad request exception.
     *
     * @param message the message
     */
    public MyBadRequestException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new my bad request exception.
     *
     * @param cause the cause
     */
    public MyBadRequestException(final Throwable cause) {
        super(cause);
    }

}
