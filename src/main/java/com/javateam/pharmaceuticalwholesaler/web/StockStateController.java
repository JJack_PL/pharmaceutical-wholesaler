package com.javateam.pharmaceuticalwholesaler.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.javateam.pharmaceuticalwholesaler.model.StockState;
import com.javateam.pharmaceuticalwholesaler.service.ProductService;
import com.javateam.pharmaceuticalwholesaler.service.StockStateService;

/**
 * The REST endpoint for {@link StockState stock states}.
 */
@Controller
@RequestMapping(path = "stockStates")
public class StockStateController {

    /** The stock state service. */
    @Autowired
    private StockStateService stockStateService;

    /** The product service. */
    @Autowired
    private ProductService productService;

    /**
     * Gets the stock state by product.
     *
     * @param productId the product id
     * @return the by product or an exception
     */
    @GetMapping("/product/{id}")
    @ResponseBody
    public StockState getByProduct(final @PathVariable("id") Long productId) {
        return this.stockStateService.getByProduct(this.productService.getById(productId));
    }

    /**
     * Creates the stock state.
     *
     * @param resource the resource
     * @return the stock state
     */
    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public StockState createStockState(final @RequestBody @Valid StockState stockState) {
        RestPreconditions.checkRequestElementNotNull(stockState, "StockState is null");
        RestPreconditions.checkRequestState(stockState.getId() == null, "StockState id is not null");

        return this.stockStateService.createStockState(stockState);
    }

    /**
     * Update stock state.
     *
     * @param stockStateId the stock state id
     * @param stockState the stock state
     * @return the stock state
     */
    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public StockState updateStockState(final @PathVariable("id") Long stockStateId,
        @RequestBody @Valid final StockState stockState) {
        RestPreconditions.checkRequestElementNotNull(stockState, "StockState is null");
        RestPreconditions.checkRequestElementNotNull(stockState.getId(), "StockState id is null");
        RestPreconditions.checkRequestState(stockState.getId().equals(stockStateId),
            "StockState id need to be the same like in the request");

        return this.stockStateService.updateStockState(stockState);
    }

    /**
     * List of products.
     *
     * @return the list of products
     */
    @GetMapping
    @ResponseBody
    public List<StockState> listStockStates() {
        return this.stockStateService.findAll();
    }

}
