package com.javateam.pharmaceuticalwholesaler.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.service.ProductService;


/**
 * The REST endpoint for {@link Product products}.
 */
@Controller
@RequestMapping(path = "products")
public class ProductController {

    /** The product service. */
    @Autowired
    private ProductService productService;
    
    /**
     * Creates the product.
     *
     * @param product the product
     * @return the product
     */
    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Product createProduct(final @RequestBody @Valid Product product) {
        return this.productService.createProduct(product);
    }

    /**
     * List of products.
     *
     * @return the list of products
     */
    @GetMapping
    @ResponseBody
    public List<Product> listProducts() {
        return this.productService.findAll();

    }



}
