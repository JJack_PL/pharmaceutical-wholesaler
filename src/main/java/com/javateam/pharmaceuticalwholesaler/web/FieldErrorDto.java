package com.javateam.pharmaceuticalwholesaler.web;

/**
 * Represents field error.
 */
public class FieldErrorDto {

    /** The field. */
    private final String field;

    /** The error message. */
    private final String message;

    /**
     * Instantiates a new field error dto.
     *
     * @param field the field
     * @param message the message
     */
    public FieldErrorDto(final String field, final String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return this.field;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return "FieldErrorDto [field=" + this.field + ", message=" + this.message + "]";
    }

}
