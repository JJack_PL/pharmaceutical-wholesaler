package com.javateam.pharmaceuticalwholesaler.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.service.CustomerService;

/**
 * The REST endpoint for {@link Customer customers}.
 */
@Controller
@RequestMapping(path = "customers")
public class CustomerController {

    /** The customer service. */
    @Autowired
    private CustomerService customerService;

    /**
     * Creates the customer.
     *
     * @param customer the customer
     * @return the customer
     */
    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Customer createCustomer(final @RequestBody @Valid Customer customer) {
        return this.customerService.createCustomer(customer);
    }

    /**
     * List of customers.
     *
     * @return the list of customers
     */
    @GetMapping
    @ResponseBody
    public List<Customer> listCustomers() {
        return this.customerService.findAll();
    }

}
