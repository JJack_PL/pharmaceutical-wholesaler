package com.javateam.pharmaceuticalwholesaler.web;

import java.util.Objects;

public class ApiError {

    private int status;

    private String message;

    private String developerMessage;

    public ApiError(final int status, final String message, final String developerMessage) {
        this.status = status;
        this.message = message;
        this.developerMessage = developerMessage;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getDeveloperMessage() {
        return this.developerMessage;
    }

    public void setDeveloperMessage(final String developerMessage) {
        this.developerMessage = developerMessage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.developerMessage, this.message, this.status);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
        ApiError other = (ApiError) obj;
        return Objects.equals(this.developerMessage, other.developerMessage)
            && Objects.equals(this.message, other.message)
            && this.status == other.status;
    }

    @Override
    public String toString() {
        return "ApiError [status=" + this.status + ", message=" + this.message + ", developerMessage="
            + this.developerMessage + "]";
    }

}
