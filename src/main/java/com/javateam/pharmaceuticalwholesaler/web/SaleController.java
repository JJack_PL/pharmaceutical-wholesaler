package com.javateam.pharmaceuticalwholesaler.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.Sale;
import com.javateam.pharmaceuticalwholesaler.service.CustomerService;
import com.javateam.pharmaceuticalwholesaler.service.ProductService;
import com.javateam.pharmaceuticalwholesaler.service.SaleService;

/**
 * The REST endpoint for {@link Sale sales}.
 */
@Controller
@RequestMapping(path = "sales")
public class SaleController {

    /** The sale service. */
    @Autowired
    private SaleService saleService;

    /** The product service. */
    @Autowired
    private ProductService productService;

    /** The customer service. */
    @Autowired
    private CustomerService customerService;

    /**
     * Creates the sale.
     *
     * @param sale the sale
     * @return the sale
     */
    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Sale createSale(final @RequestBody @Valid Sale sale) {
        return this.saleService.createSale(sale);
    }

    /**
     * List of sales.
     *
     * @return the list of sales
     */
    @GetMapping
    @ResponseBody
    public List<Sale> listSales() {
        return this.saleService.findAll();
    }

    /**
     * List sales by customer.
     *
     * @param customer the customer
     * @return the list
     */
    @GetMapping("/history/customer/{id}")
    @ResponseBody
    public List<Sale> listSalesByCustomer(final @PathVariable("id") Long customerId) {
        Customer customer = this.customerService.getById(customerId);

        return this.saleService.findByCustomer(customer, Sale.DEFAULT_SORT);
    }

    /**
     * List sales by product.
     *
     * @param productId the product id
     * @return the list
     */
    @GetMapping("/history/product/{id}")
    @ResponseBody
    public List<Sale> listSalesByProduct(final @PathVariable("id") Long productId) {
        Product product = this.productService.getById(productId);
        return this.saleService.findByProductDescDateTime(product);
    }

}
