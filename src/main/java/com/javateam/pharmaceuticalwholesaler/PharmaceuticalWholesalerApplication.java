package com.javateam.pharmaceuticalwholesaler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(info = @Info(title = "Pharmaceutical wholesaler API", version = "1.0"))
@SpringBootApplication
public class PharmaceuticalWholesalerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PharmaceuticalWholesalerApplication.class, args);
	}

}
