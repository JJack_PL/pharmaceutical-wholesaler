package com.javateam.pharmaceuticalwholesaler.service.exception;


/**
 * Throws when there is not enough products on the stock.
 */
public class NotEnoughProductOnStockException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7871199007303618399L;
    
    /**
     * Constructs a new <code>EntityNotFoundException</code> exception with the
     * specified detail message.
     *
     * @param message the detail message.
     */
    public NotEnoughProductOnStockException(final String message) {
        super(message);
    }

}
