package com.javateam.pharmaceuticalwholesaler.service.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Throws when there is an exception related with stock state.
 */
public class StockStateException extends RuntimeException {

    /** The not enough product on stock exceptions. */
    private final List<NotEnoughProductOnStockException> notEnoughProductOnStockExceptions = new ArrayList<>();

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -627445541114626234L;

    /**
     * Instantiates a new stock state exception.
     *
     * @param message the message
     */
    public StockStateException(final String message) {
        super(message);
    }

    /**
     * Adds the not enough product on stock exception.
     *
     * @param ex the ex
     */
    public void addNotEnoughProductOnStockException(final NotEnoughProductOnStockException ex) {
        this.notEnoughProductOnStockExceptions.add(ex);
    }

    public List<NotEnoughProductOnStockException> getNotEnoughProductOnStockExceptions() {
        return notEnoughProductOnStockExceptions;
    }

}
