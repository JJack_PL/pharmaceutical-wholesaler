package com.javateam.pharmaceuticalwholesaler.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.SaleDao;
import com.javateam.pharmaceuticalwholesaler.model.Customer;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.Sale;
import com.javateam.pharmaceuticalwholesaler.model.SaleItem;
import com.javateam.pharmaceuticalwholesaler.service.exception.NotEnoughProductOnStockException;
import com.javateam.pharmaceuticalwholesaler.service.exception.StockStateException;

@Service
@Transactional
public class SaleService {

    /** The sale dao. */
    @Autowired
    private SaleDao saleDao;

    /** The stock state service. */
    @Autowired
    private StockStateService stockStateService;

    /**
     * Creates the sale.
     *
     * @param sale the sale
     * @return the sale
     */
    public Sale createSale(final Sale sale) {
        StockStateException stockStateException = null;
        for (SaleItem saleItem : sale.getSaleItems()) {
            try {
                this.stockStateService.removeProductFromStockState(saleItem.getProduct(), saleItem.getQuantity());
            } catch (NotEnoughProductOnStockException ex) {
                if (stockStateException == null) {
                    stockStateException = new StockStateException("In stock there are not enough products");
                }
                stockStateException.addNotEnoughProductOnStockException(ex);
            }
        }

        if (stockStateException != null) {
            throw stockStateException;
        }
        return this.saleDao.save(sale);
    }

    /**
     * Find all sales.
     *
     * @return the list of sales
     */
    public List<Sale> findAll() {
        return this.saleDao.findAll(Sale.DEFAULT_SORT);
    }

    /**
     * Find by customer.
     *
     * @param sort the sort
     * @return the list of sales for the given customer
     */
    public List<Sale> findByCustomer(final Customer customer, final Sort sort) {
        return this.saleDao.findByCustomer(customer, sort);
    }

    /**
     * Find by product desc date time.
     *
     * @param product the product
     * @return the list of sales by product sorted descending by date time
     */
    public List<Sale> findByProductDescDateTime(final Product product) {
        return this.saleDao.findByProductDescDateTime(product);
    }

}
