package com.javateam.pharmaceuticalwholesaler.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.StockStateDao;
import com.javateam.pharmaceuticalwholesaler.dao.exception.MyEntityNotFoundException;
import com.javateam.pharmaceuticalwholesaler.model.Product;
import com.javateam.pharmaceuticalwholesaler.model.StockState;
import com.javateam.pharmaceuticalwholesaler.service.exception.NotEnoughProductOnStockException;

@Service
@Transactional
public class StockStateService {

    /** The stock state dao. */
    @Autowired
    private StockStateDao stockStateDao;

    /**
     * Gets the stock state by product.
     *
     * @param product the product
     * @return the by product or throws an exception
     */
    public StockState getByProduct(final Product product) {
        return this.stockStateDao.findByProduct(product).orElseThrow(() -> new MyEntityNotFoundException(
            "StockState with product with the id=" + product.getId() + " does not exist"));
    }

    /**
     * Creates the stock state.
     *
     * @param stockState the stock state
     * @return the new created stock state
     */
    public StockState createStockState(final StockState stockState) {
        return this.stockStateDao.save(stockState);
    }

    /**
     * Update the stock state.
     *
     * @param stockState the stock state
     * @return the stock state
     */
    public StockState updateStockState(final StockState stockState) {
        return this.stockStateDao.save(stockState);
    }

    /**
     * Find all stock states.
     *
     * @return the list of all stock states
     */
    public List<StockState> findAll() {
        return this.stockStateDao.findAll(StockState.DEFAULT_SORT);
    }

    /**
     * Removes the product from stock state and return remaining quantity.
     *
     * @param product the product
     * @param quantityToRemove the quantity to remove
     * @return the remaining quantity (never lower then 0)
     * @throws NotEnoughProductOnStockException the not enough product on stock exception
     */
    public int removeProductFromStockState(final Product product, final int quantityToRemove)
        throws NotEnoughProductOnStockException {
        Optional<StockState> stockState = this.stockStateDao.findByProduct(product);

        if (stockState.isPresent()) {
            if (quantityToRemove > stockState.get().getQuantity()) {
                throw new NotEnoughProductOnStockException(
                    "There is only " + stockState.get().getQuantity()
                        + " products on stock so there is no way to remove " + quantityToRemove + " from it. "
                        + product);
            } else {
                stockState.get().removeQuantity(quantityToRemove);
                return stockState.get().getQuantity();
            }
        } else {
            throw new NotEnoughProductOnStockException(
                "There is no stock state for the product: " + product);
        }
    }

}
