package com.javateam.pharmaceuticalwholesaler.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.CustomerDao;
import com.javateam.pharmaceuticalwholesaler.dao.exception.MyEntityNotFoundException;
import com.javateam.pharmaceuticalwholesaler.model.Customer;

@Service
@Transactional
public class CustomerService {

    /** The customer dao. */
    @Autowired
    private CustomerDao customerDao;

    /**
     * Creates the customer.
     *
     * @param customer the customer
     * @return the created customer
     */
    public Customer createCustomer(final Customer customer) {
        return this.customerDao.save(customer);
    }

    /**
     * Find all customers.
     *
     * @return the list of customers
     */
    public List<Customer> findAll() {
        return this.customerDao.findAll(Customer.DEFAULT_SORT);
    }

    /**
     * Gets the consumer by id.
     *
     * @param id the id
     * @return the by id
     */
    public Customer getById(final Long id) {
        return this.customerDao.findById(id)
            .orElseThrow(() -> new MyEntityNotFoundException("Customer with the id=" + id + " does not exist"));
    }

}
