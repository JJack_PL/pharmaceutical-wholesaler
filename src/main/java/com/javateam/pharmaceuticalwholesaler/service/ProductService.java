package com.javateam.pharmaceuticalwholesaler.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javateam.pharmaceuticalwholesaler.dao.ProductDao;
import com.javateam.pharmaceuticalwholesaler.dao.exception.MyEntityNotFoundException;
import com.javateam.pharmaceuticalwholesaler.model.Product;

@Service
@Transactional
public class ProductService {

    /** The product dao. */
    @Autowired
    private ProductDao productDao;

    /**
     * Creates the product.
     *
     * @param product the product
     * @return the product
     */
    public Product createProduct(final Product product) {
        return this.productDao.save(product);
    }

    /**
     * Find all products.
     *
     * @return the list
     */
    public List<Product> findAll() {
        return this.productDao.findAll(Product.DEFAULT_SORT);

    }

    /**
     * Gets the product by id.
     *
     * @param id the id
     * @return the by id
     */
    public Product getById(final Long id) {
        return this.productDao.findById(id)
            .orElseThrow(() -> new MyEntityNotFoundException("Product with the id=" + id + " does not exist"));
    }

}
